//
//  ColorPickViewController.swift
//  PUDM
//
//  Created by Reece Kenney on 08/04/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class ColorPickViewController: UIViewController {
    
    var defaults = NSUserDefaults() //User defaults
    var colorTrack:String! //Holds user's color track

    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorTrack = defaults.stringForKey("colorTrack") //Get the color track for user
        
        // Do any additional setup after loading the view.
    }
    
    //**** Set colors for color track *******
    @IBAction func button1(sender: AnyObject) {
        defaults.setValue("orange", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button2(sender: AnyObject) {
        defaults.setValue("mint", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button3(sender: AnyObject) {
        defaults.setValue("pink", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button4(sender: AnyObject) {
        defaults.setValue("yellow", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button5(sender: AnyObject) {
        defaults.setValue("red", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button6(sender: AnyObject) {
        defaults.setValue("blue", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button7(sender: AnyObject) {
        defaults.setValue("silver", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func button8(sender: AnyObject) {
        defaults.setValue("brown", forKey: "colorTrack")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
