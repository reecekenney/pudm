//
//  FirstViewController.swift
//  PUDM
//
//  Created by Reece Kenney on 30/03/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MWFeedParserDelegate  {
    
    
    @IBOutlet weak var countdownLabel: UILabel! //Label for live countdown
    @IBOutlet weak var tableView: UITableView! //Table view for annoucements etc
    
    var defaults = NSUserDefaults() //User defaults
    var username:String! //Holds username of user of device
    var colorTrack:String! //Holds user's color track
    var feedItems = [MWFeedItem]() //Holds all items for tableview
    var linkToOpen:String! //Holds link to send to next webview view controller
    var timer = NSTimer() //Timer for countdown to date 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        request("http://www.pudm.org/category/announcements/feed/")
    }
    
    override func viewDidAppear(animated: Bool) {
        
        username = defaults.stringForKey("username") //Get the username for user
        colorTrack = defaults.stringForKey("colorTrack") //Get the color track for user
    }
    
    @IBAction func donateButton(sender: AnyObject) {
        
        if username != nil {
            openURL("http://donate.pudm.org/" + username)
        }
        else {
            openURL("http://donate.pudm.org")
        }
        
    }
    
    @IBAction func eventInfoButton(sender: AnyObject){
        openURL("http://donate.rileykids.org/site/TR/DanceMarathon/DanceMarathons?pg=entry&fr_id=2190")
    }
    
    func openURL(url:String) {
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }
    
    func feedParserDidStart(parser: MWFeedParser!) {
        feedItems = [MWFeedItem]()
    }
    
    func feedParserDidFinish(parser: MWFeedParser!) {
        tableView.reloadData()
    }
    
    func feedParser(parser: MWFeedParser!, didParseFeedItem item: MWFeedItem!) {
        feedItems.append(item)
    }
    
    func request(url:String) {
        let url = NSURL(string: url)
        let feedParser = MWFeedParser(feedURL: url)
        feedParser.delegate = self
        feedParser.parse()
    }
    
    func parserDidEndDocument(parser: NSXMLParser!) {
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return feedItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! AnnouncementsCell
        
        let item = feedItems[indexPath.row] as MWFeedItem
        cell.announcementText.text = item.title
        
        let formatter = NSDateFormatter() //Date formatter
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        cell.announcementDate.text = formatter.stringFromDate(item.date)
        
        let formatter2 = NSDateFormatter() //Date formatter
        formatter2.dateFormat = "d"
        cell.calendarDate.text = formatter2.stringFromDate(item.date)
        
        
        

        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let item = feedItems[indexPath.row] as MWFeedItem
        linkToOpen = item.identifier
        
        self.performSegueWithIdentifier("OpenLink", sender: self)
    }
    
    

    //Called before view segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {

        let destinationVC = segue.destinationViewController as! LinkViewController
        destinationVC.link = linkToOpen
    }



}