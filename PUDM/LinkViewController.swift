//
//  LinkViewController.swift
//  PUDM
//
//  Created by Reece Kenney on 06/04/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class LinkViewController: UIViewController {
    
    @IBOutlet weak var linkWebView: UIWebView!
    
    
    var link: String!
    
    
    func viewDidAppear() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func dismissView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        linkWebView.loadRequest(NSURLRequest(URL: NSURL(string: link)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}