//
//  QRViewController.swift
//  PUDM
//
//  Created by Reece Kenney on 08/04/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var messageLabel:UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    var defaults = NSUserDefaults() //User defaults
    var username:String! //Career account username for user
    var colorTrack:String! //Holds user's color track
    var linkToOpen:String! //Holds link to open (passed from other view)
    var itemScanned = false //True if link has just been scanned
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //Check if user has internet connection
        if Network.isConnectedToNetwork() == false {
            
            var inputTextField: UITextField?
            let accountPrompt = UIAlertController(title: "No internet connection found", message: "This app requires a working internet connection", preferredStyle: UIAlertControllerStyle.Alert)
            accountPrompt.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.tabBarController!.selectedIndex = 0;
            }))
            
            presentViewController(accountPrompt, animated: true, completion: nil)
            
        } else {
        
            itemScanned = false
            username = defaults.stringForKey("username") //Get the username for user
            colorTrack = defaults.stringForKey("colorTrack") //Get the color track for user
            
            // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
            // as the media type parameter.
            let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
            
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            var error:NSError?
            let input: AnyObject! = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: &error)
            
            if (error != nil) {
                // If any error occurs, simply log the description of it and don't continue any more.
                println("\(error?.localizedDescription)")
                return
            }
            
            if username == nil {
                //Show alert box
                let colorPrompt = UIAlertController(title: "Please sign in", message: "You must sign in with your Purdue career account in order to gain color wars points.", preferredStyle: UIAlertControllerStyle.Alert)
                colorPrompt.addAction(UIAlertAction(title: "Not now", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 0;
                }))
                
                colorPrompt.addAction(UIAlertAction(title: "Sign in", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 2;
                }))
                
                self.presentViewController(colorPrompt, animated: true, completion: nil)
            }
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input as! AVCaptureInput)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Move the message label to the top view
            view.bringSubviewToFront(messageLabel)
            view.bringSubviewToFront(instructionLabel)
            
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            qrCodeFrameView?.layer.borderColor = UIColor.greenColor().CGColor
            qrCodeFrameView?.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView!)
            view.bringSubviewToFront(qrCodeFrameView!)
            
        }//End check for internet connection ELSE BLOCK
    }
    
    override func viewDidDisappear(animated: Bool) {
        captureSession?.stopRunning()
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero
            messageLabel.text = "No QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil && itemScanned == false {
                
                itemScanned = true
                messageLabel.text = "Got it!"
                linkToOpen = metadataObj.stringValue + "&purdue_ca=" + username
                
                //If lined webpage contains string "pudm"
                if linkToOpen.lowercaseString.rangeOfString("pudm") != nil {
                    //self.performSegueWithIdentifier("OpenLink", sender: self)
                    executeScript()
                }
                else {
                    messageLabel.text = "Invalid QR Code"
                }
                
            }
        }
        
        let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
        
        qrCodeFrameView?.frame = barCodeObject.bounds
    }
    
    func executeScript() {
        
        let url = NSURL(string: linkToOpen)
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0)
        println(linkToOpen)
    
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            if(( NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString.rangeOfString( "points added")) != nil) {
                
                //Show alert box
                let colorPrompt = UIAlertController(title: "Success", message: "Points added! Press OK to go to your myPUDM page.", preferredStyle: UIAlertControllerStyle.Alert)
                colorPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 2;
                }))
                self.presentViewController(colorPrompt, animated: true, completion: nil)
            }
            else if(( NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString.rangeOfString( "points deducted")) != nil) {
                
                //Show alert box
                let colorPrompt = UIAlertController(title: "Success", message: "Transaction succeeded! Press OK to go to your myPUDM page.", preferredStyle: UIAlertControllerStyle.Alert)
                colorPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 2;
                }))
                self.presentViewController(colorPrompt, animated: true, completion: nil)
            }
            else if(( NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString.rangeOfString( "deduction failed")) != nil) {
                
                //Show alert box
                let colorPrompt = UIAlertController(title: "Transaction failed", message: "Your transaction could not be completed because you do not have enough points.", preferredStyle: UIAlertControllerStyle.Alert)
                colorPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 2;
                }))
                self.presentViewController(colorPrompt, animated: true, completion: nil)
            }
            else if(( NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString.rangeOfString( "points already added")) != nil) {
                
                //Show alert box
                let colorPrompt = UIAlertController(title: "Points already added", message: "You have already checked-in to this event! Press OK to go to your myPUDM page.", preferredStyle: UIAlertControllerStyle.Alert)
                colorPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 2;
                }))
                self.presentViewController(colorPrompt, animated: true, completion: nil)
            }
            else {
                println(NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString)
                //Show alert box
                let colorPrompt = UIAlertController(title: "Something went wrong...", message: "Something wrong happened when checking in. Please report this to your Executive.", preferredStyle: UIAlertControllerStyle.Alert)
                colorPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) -> Void in
                    self.itemScanned = false
                    self.tabBarController!.selectedIndex = 2;
                }))
                self.presentViewController(colorPrompt, animated: true, completion: nil)
            }
            
            
        }
    
    }
    
    func dataOfJson(url: String) -> NSArray {
        var data = NSData(contentsOfURL: NSURL(string: url)!)
        return (NSJSONSerialization.JSONObjectWithData(data!, options: nil, error: nil) as! NSArray)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        let touchPer = touches.first as! UITouch
        let screenSize = UIScreen.mainScreen().bounds.size
        var focus_x = touchPer.locationInView(self.view).x / screenSize.width
        var focus_y = touchPer.locationInView(self.view).y / screenSize.height
        
        if let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) {
            device.lockForConfiguration(nil)
            
            if(device.lockForConfiguration(nil)) {
                device.focusMode = AVCaptureFocusMode.ContinuousAutoFocus
                
                device.focusPointOfInterest = CGPointMake(focus_x, focus_y)
                device.exposureMode = AVCaptureExposureMode.ContinuousAutoExposure
                device.unlockForConfiguration()
                
                device.focusPointOfInterest = CGPointMake(focus_x, focus_y)
                device.focusMode = AVCaptureFocusMode.AutoFocus
                device.exposurePointOfInterest = CGPointMake(focus_x, focus_y)
                device.exposureMode = AVCaptureExposureMode.ContinuousAutoExposure
            }
        }
    }
    
    //Called before view segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        let destinationVC = segue.destinationViewController as! LinkViewController
        destinationVC.link = linkToOpen
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

