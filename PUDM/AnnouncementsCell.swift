//
//  announcementsCell.swift
//  PUDM
//
//  Created by Reece Kenney on 31/03/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class AnnouncementsCell: UITableViewCell {


    @IBOutlet weak var announcementDate: UILabel!
    @IBOutlet weak var announcementText: UILabel!
    @IBOutlet weak var calendarDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
