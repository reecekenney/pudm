//
//  MyPUDMCellTableViewCell.swift
//  PUDM
//
//  Created by Reece Kenney on 17/04/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class MyPUDMCellTableViewCell: UITableViewCell {

    @IBOutlet weak var eventLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
