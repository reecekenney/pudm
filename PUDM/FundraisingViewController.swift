var goal100:Float!//
//  FundraisingViewController.swift
//  PUDM
//
//  Created by Aharon Hannan on 4/18/15.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit
import Foundation

class FundraisingViewController: UIViewController {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameSubtitle: UILabel!
    @IBOutlet var goalString: UILabel!
    @IBOutlet var raisedString: UILabel!
    
    var defaults = NSUserDefaults() //User defaults
    var username:String! //Career account username
    var name:String! //Holds users first and last name
    var colorTrack:String! //Holds user's color track
    var committee:String! //Holds user's committee name
    var totalPoints:String! //Holds user's total points
    var redeemablePoints:String! //Holds user's redeemable points
    
    var circleBoundarySize:CGSize!
    var circleBoundary:UIImageView!
    var circleBoundary2:UIImageView!
    
    var raised:String!
    var goal: String!
    var goalInt:Float!
    var raisedInt:Float!
    var percent: Float!
    var fundraisingData:[String]!
    
    var goal100:Float!
    var goal75:Float!
    var goal55:Float!
    var goal35:Float!
    var goal20:Float!
    
    private let floatPi = CGFloat(M_PI)
    var progressColor = UIColor(red: 45/255, green: 151/255, blue: 212/255, alpha: 1)
    var progressBackgroundColor = UIColor(red: 89/255, green: 89/255, blue: 89/255, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add progress circle area
        if(view.frame.height == 480){ //If iPhone 4 size screen
            circleBoundarySize = CGSize(width: 200, height: 200)
            circleBoundary = UIImageView(frame: CGRect(origin: CGPointMake(60, nameLabel.frame.maxY + 30), size: circleBoundarySize))
        }
        else { //If not iPhone 4 screen size
            circleBoundarySize = CGSize(width: view.frame.width - 40, height: view.frame.width - 40)
            circleBoundary = UIImageView(frame: CGRect(origin: CGPointMake(20, nameLabel.frame.maxY + 30), size: circleBoundarySize))
        }
        circleBoundary2 = UIImageView(frame: circleBoundary.frame)
        self.view.addSubview(circleBoundary)
        self.view.addSubview(circleBoundary2)
        
        nameSubtitle.center = CGPointMake(0, 0)
        
    }
    func drawPercentCircle(size: CGSize, percentage: Float, color: UIColor, lineWidth: CGFloat) -> UIImage {
        // Setup our context
        let bounds = CGRect(origin: CGPoint.zeroPoint, size: size)
        let opaque = false
        let scale: CGFloat = 0
        
        UIGraphicsBeginImageContextWithOptions(size, opaque, scale)
        let context = UIGraphicsGetCurrentContext()
        let origo = CGPointMake(size.width / 2, size.height / 2)
        let radius: CGFloat = bounds.height / 2 - 14 / 2
        CGContextSetLineWidth(context, lineWidth)
        CGContextMoveToPoint(context, size.width / 2, lineWidth / 2)
        CGContextAddArc(context, origo.x, origo.y, radius, floatPi * 3 / 2, floatPi * 3 / 2 + floatPi * 2 * CGFloat(percentage / 100), 0)
        color.setStroke()
        let lastPoint = CGContextGetPathCurrentPoint(context)
        CGContextStrokePath(context)
    
        
        // Drawing complete, retrieve the finished image and cleanup
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    override func viewDidAppear(animated: Bool) {
        
        nameLabel.text = "Please Wait"
        nameSubtitle.text = "Retrieving fundraising data..."
        nameSubtitle.font = UIFont(name: nameSubtitle.font.fontName, size: 20)
        goalString.hidden = true
        raisedString.hidden = true
        
        //Check if user has internet connection
        if Network.isConnectedToNetwork() == false {
            
            var inputTextField: UITextField?
            let accountPrompt = UIAlertController(title: "No internet connection found", message: "This app requires a working internet connection", preferredStyle: UIAlertControllerStyle.Alert)
            accountPrompt.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.tabBarController!.selectedIndex = 0;
            }))
            
            presentViewController(accountPrompt, animated: true, completion: nil)
            
        } else {
        
            username = defaults.stringForKey("username")
            
            if(username == nil) {
                
                nameLabel.hidden = true
                nameSubtitle.hidden = true
                
                //Show career account prompt/alert
                showAlert("Connect to myPUDM", message: "Enter your Purdue Career Account username to associate your phone with earning points.")
            }
            else {
                getUserInfo(username)
                
            }
        }
    }
    @IBAction func openFundraisingPage(sender: AnyObject) {
        openURL("http://donate.pudm.org/" + username)
    }
    
    func getUserInfo(user:String) {
        let url = NSURL(string: "http://my.pudm.org/ios/iosuser.php?username=" + user) //Link with username parameter
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0)
        
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            if(error != nil){
                var inputTextField: UITextField?
                let accountPrompt = UIAlertController(title: "An Error Occurred", message: "Check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
                accountPrompt.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    self.tabBarController!.selectedIndex = 0;
                }))
                
                self.presentViewController(accountPrompt, animated: true, completion: nil)
            }
            else {
                if(( NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString.rangeOfString( "user found")) != nil) {
                    
                    var userData = NSString(data: data, encoding: NSASCIIStringEncoding)!.componentsSeparatedByString("UserData:")
                    
                    //Set global variable values
                    self.username = user//Save account username to global variable
                    self.defaults.setValue(self.username, forKey: "username") //Set username user default
                    self.name = (userData[1] as! String) + " " + (userData[2] as! String)
                    self.committee = (userData[3] as! String)
                    self.colorTrack = (userData[4] as! String)
                    self.totalPoints = (userData[5] as! String)
                    self.redeemablePoints = (userData[6] as! String)
                    
                    self.defaults.setValue(self.colorTrack, forKey:"colorTrack") //Set color track user default
                    
                    //Get fundraising details
                    self.fetchData()
                    
                }
                else {
                    self.showAlert("Username not found", message: "The username \"\(user)\" was not found in our records. Please check the spelling and try again.")
                }
            }
            
        }
        
    }
    
    //Alert boxes if account not connected
    func showAlert(title:String, message:String) {
        var inputTextField: UITextField?
        let accountPrompt = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        accountPrompt.addAction(UIAlertAction(title: "Not now", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            self.tabBarController!.selectedIndex = 0;
        }))
        
        accountPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            var usernameEntered = inputTextField!.text.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            if usernameEntered == ""  {
                self.showAlert("Enter a username", message:"Please enter a career account username.")
            }
            else {
                self.getUserInfo(usernameEntered)
            }
        }))
        accountPrompt.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Purdue Career Account Username"
            inputTextField = textField
        })
        
        presentViewController(accountPrompt, animated: true, completion: nil)
    }
    
    func showLinkAlert(title:String, message:String) {
        var inputTextField: UITextField?
        let accountPrompt = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        accountPrompt.addAction(UIAlertAction(title: "Not now", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            self.tabBarController!.selectedIndex = 0;
        }))
        
        accountPrompt.addAction(UIAlertAction(title: "Sign Up", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            self.openURL("http://www.pudm.org/fundraising/pyoudm/")
        }))
        
        presentViewController(accountPrompt, animated: true, completion: nil)
    }
    //
    func updateLabels(){
        //Unhide labels
        goalString.hidden = false
        raisedString.hidden = false
        nameLabel.hidden = false
        raisedString.hidden = false
        nameSubtitle.hidden = false
        goalString.hidden = false
        
        nameLabel.text = username.uppercaseString
        raisedString.text = "$" + self.raised
        goalString.text = "Goal: $" + self.goal
        nameSubtitle.text = String(format: "%.0f", self.percent) + "%"
        nameSubtitle.font = UIFont(name: nameSubtitle.font.fontName, size: 83)
        
        //Draw Progress Circle
        let backCircle = drawPercentCircle(circleBoundarySize, percentage: 100, color: progressBackgroundColor, lineWidth: 2)
        let frontCircle = drawPercentCircle(circleBoundarySize, percentage: self.percent, color: progressColor, lineWidth: 14)
        
        circleBoundary.image = backCircle
        circleBoundary2.image = frontCircle
    }
    
    func fetchData(){
        //Connect to pYOUdm Scraper
        let url = NSURL(string: "http://my.pudm.org/ios/scrape.php?pyoudm=" + username)
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            //Array split by :; strings - see PHP file for method of parsing
            self.fundraisingData = NSString(data: data, encoding: NSASCIIStringEncoding)!.componentsSeparatedByString(":;") as! [String]
            
            if self.fundraisingData.count == 1 {
                
                //Update labels
                self.nameLabel.text = "pYOUdm Not Found"
                self.nameSubtitle.text = "Please sign up for one at PUDM.org"
                
                self.showLinkAlert("pYOUdm Not Found", message: "We can't seem to fetch your fundraising details since you haven't requested a pYOUdm. Please sign up for one, and check back later.")
                return
            }
            
            println(self.fundraisingData)
            //"Goal:" and "Achieved:" are in elements 0 and 2 respectively
            self.goal = self.fundraisingData[1]
            self.raised = self.fundraisingData[3]
            
            // Calculations Section
            
            self.goalInt = (self.goal as NSString).floatValue
            self.raisedInt = (self.raised as NSString).floatValue
            
            self.goal20 = 0.2 * self.goalInt
            self.goal35 = 0.35 * self.goalInt
            self.goal55 = 0.55 * self.goalInt
            self.goal75 = 0.75 * self.goalInt
            self.goal100 = self.goalInt
            
            self.percent = self.raisedInt / self.goalInt * 100
            
            //Update labels
            self.updateLabels()
            
        }
    }
    
    
    
    func openURL(url:String) {
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }
    
}
