//
//  MyPUDMViewController.swift
//  PUDM
//
//  Created by Reece Kenney on 15/04/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class MyPUDMViewController: UIViewController {

    
    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var tabButtons: UISegmentedControl!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var colorCommitteeLabel: UILabel!
    @IBOutlet var redeemableLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var pointsEarnedSubtitle: UILabel!
    @IBOutlet var rewardSubtitle: UILabel!
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    var defaults = NSUserDefaults() //User defaults
    var username:String! //Career account username
    var name:String! //Holds users first and last name
    var colorTrack:String! //Holds user's color track
    var committee:String! //Holds user's committee name
    var totalPoints:String! //Holds user's total points
    var redeemablePoints:String! //Holds user's redeemable points
    
    var events:[String]! //Holds events containing name, date and points for that event
    var eventNames = [String]() //Event names parsed from database
    var eventDates = [String]() //Event dates parsed from database
    var eventPoints = [String]() //Event points parsed from database

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Hide labels if not signed in to myPUDM
        nameLabel.hidden = true
        colorCommitteeLabel.hidden = true
        self.redeemableLabel.hidden = true
        self.totalLabel.hidden = true
        self.pointsEarnedSubtitle.hidden = true
        self.rewardSubtitle.hidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        //Check if user has internet connection
        if Network.isConnectedToNetwork() == false {
            
            var inputTextField: UITextField?
            let accountPrompt = UIAlertController(title: "No internet connection found", message: "This app requires a working internet connection", preferredStyle: UIAlertControllerStyle.Alert)
            accountPrompt.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.tabBarController!.selectedIndex = 0;
            }))
            
            presentViewController(accountPrompt, animated: true, completion: nil)
            
        } else {
            username = defaults.stringForKey("username")
            
            if(username == nil) {
                
                //Show career account prompt/alert
                showAlert("Connect to myPUDM", message: "Enter your Purdue Career Account username to associate your phone with earning points.")
            }
            else {
                getUserInfo(username)
                displayData("transactions")
            }
            
            eventsTableView.reloadData()
        }
    }
    
    @IBAction func controlChange(sender: AnyObject) {
        
        if segmentedControl.selectedSegmentIndex == 0{
            displayData("transactions")
        }
        else{
            displayData("earned")
        }
        eventsTableView.reloadData()
    }
    
    func displayData(method:String) {
        
        let url = NSURL(string: "http://my.pudm.org/ios/audit.php?purdue_ca=" + username + "&method=" + method) //Link with username parameter
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            //Clear arrays first
            self.eventNames = [String]()
            self.eventDates = [String]()
            self.eventPoints = [String]()
            
            self.events = NSString(data: data, encoding: NSASCIIStringEncoding)!.componentsSeparatedByString("Item:") as! [String]
            
            var count = 0
            for event in self.events {
                
                //Skip first element
                if count++ == 0 {
                    continue
                }
                var array = event.componentsSeparatedByString("::")
                
                self.eventNames.append(array[0])
                self.eventDates.append(array[1])
                self.eventPoints.append(array[2])
                
                
            }
            //reverse arrays to put most recent first
            //self.eventNames = self.eventNames.reverse()
            //self.eventDates = self.eventDates.reverse()
            //self.eventPoints = self.eventPoints.reverse()
            
            self.eventsTableView.reloadData()
        }
        
        
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func getUserInfo(user:String) {
        let url = NSURL(string: "http://mypudm.pudm.org/ios/iosuser.php?username=" + user) //Link with username parameter
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0)
        
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            if(error != nil){
                var inputTextField: UITextField?
                let accountPrompt = UIAlertController(title: "An Error Occurred", message: "Check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
                accountPrompt.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    self.tabBarController!.selectedIndex = 0;
                }))
                
                self.presentViewController(accountPrompt, animated: true, completion: nil)
            }
            else {
            
                if(( NSString(data: data, encoding: NSASCIIStringEncoding)!.lowercaseString.rangeOfString( "user found")) != nil) {
                    
                    var userData = NSString(data: data, encoding: NSASCIIStringEncoding)!.componentsSeparatedByString("UserData:")
                    
                    //Set global variable values
                    self.username = user//Save account username to global variable
                    self.defaults.setValue(self.username, forKey: "username") //Set username user default
                    self.name = (userData[1] as! String) + " " + (userData[2] as! String)
                    self.committee = (userData[3] as! String)
                    self.colorTrack = (userData[4] as! String)
                    self.totalPoints = (userData[5] as! String)
                    self.redeemablePoints = (userData[6] as! String)
                    
                    self.defaults.setValue(self.colorTrack, forKey:"colorTrack") //Set color track user default
                    
                    
                    
                    //Update labels
                    self.nameLabel.text = self.name.uppercaseString
                    self.colorCommitteeLabel.text = self.colorTrack + " / " + self.committee
                    
                    if (self.redeemablePoints == "")
                    {
                        //Check if the value is "NULL"
                        self.redeemableLabel.text = "0"
                    }
                    else{
                        self.redeemableLabel.text = self.redeemablePoints
                    }
                    
                    if (self.totalPoints == "")
                    {
                        //Check if the value is "NULL"
                        self.totalLabel.text = "0"
                    }
                    else{
                        self.totalLabel.text = self.totalPoints
                    }
                    
                    
                    //Unhide labels
                    self.nameLabel.hidden = false
                    self.colorCommitteeLabel.hidden = false
                    self.redeemableLabel.hidden = false
                    self.totalLabel.hidden = false
                    self.pointsEarnedSubtitle.hidden = false
                    self.rewardSubtitle.hidden = false
                    
                    //Change background color based on color track
                    
                    switch self.colorTrack{
                    case "Mint":
                        //Mint Pantone 345 C -> Hex #91d6ac
                        self.view.backgroundColor = self.UIColorFromRGB(0x91d6ac)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.blackColor()
                        self.colorCommitteeLabel.textColor = UIColor.blackColor()
                        self.redeemableLabel.textColor = UIColor.blackColor()
                        self.totalLabel.textColor = UIColor.blackColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.blackColor()
                        self.rewardSubtitle.textColor = UIColor.blackColor()
                        
                    case "Gold":
                        //Gold Pantone 1235 C -> Hex #ffb81c
                        self.view.backgroundColor = self.UIColorFromRGB(0xffb81c)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.blackColor()
                        self.colorCommitteeLabel.textColor = UIColor.blackColor()
                        self.redeemableLabel.textColor = UIColor.blackColor()
                        self.totalLabel.textColor = UIColor.blackColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.blackColor()
                        self.rewardSubtitle.textColor = UIColor.blackColor()
                        self.tabButtons.tintColor = UIColor.blackColor()
                        
                    case "Orange":
                        //Sunset Pantone 7578 C -> Hex #dc6b2f
                        self.view.backgroundColor = self.UIColorFromRGB(0xdc6b2f)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        self.redeemableLabel.textColor = UIColor.whiteColor()
                        self.totalLabel.textColor = UIColor.whiteColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.whiteColor()
                        self.rewardSubtitle.textColor = UIColor.whiteColor()
                        
                        
                    case "Blue":
                        //Royal Pantone 7686 C -> Hex #1d4f91
                        self.view.backgroundColor = self.UIColorFromRGB(0x1d4f91)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        self.redeemableLabel.textColor = UIColor.whiteColor()
                        self.totalLabel.textColor = UIColor.whiteColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.whiteColor()
                        self.rewardSubtitle.textColor = UIColor.whiteColor()
                        
                        
                    case "Purple":
                        //Lilac Pantone: 7679 C -> Hex #563d82
                        self.view.backgroundColor = self.UIColorFromRGB(0x563d82)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        self.redeemableLabel.textColor = UIColor.whiteColor()
                        self.totalLabel.textColor = UIColor.whiteColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.whiteColor()
                        self.rewardSubtitle.textColor = UIColor.whiteColor()
                        
                        
                    case "Green":
                        //Irish Green Pantone: 340 C -> Hex #00965e
                        self.view.backgroundColor = self.UIColorFromRGB(0x00965e)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        self.redeemableLabel.textColor = UIColor.whiteColor()
                        self.totalLabel.textColor = UIColor.whiteColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.whiteColor()
                        self.rewardSubtitle.textColor = UIColor.whiteColor()
                        
                        
                    case "Pink":
                        //Azalea Pantone: 224 C -> Hex #eb6fbd
                        self.view.backgroundColor = self.UIColorFromRGB(0xeb6fbd)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        
                        
                    case "Turquoise":
                        //Pantone: 7711 C -> Hex #0097a9
                        self.view.backgroundColor = self.UIColorFromRGB(0x0097a9)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        self.redeemableLabel.textColor = UIColor.whiteColor()
                        self.totalLabel.textColor = UIColor.whiteColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.whiteColor()
                        self.rewardSubtitle.textColor = UIColor.whiteColor()
                        
                        
                        
                    default:
                        self.view.backgroundColor = self.UIColorFromRGB(0x000000)
                        
                        //Change Label Colors
                        self.nameLabel.textColor = UIColor.whiteColor()
                        self.colorCommitteeLabel.textColor = UIColor.whiteColor()
                        self.redeemableLabel.textColor = UIColor.whiteColor()
                        self.totalLabel.textColor = UIColor.whiteColor()
                        self.pointsEarnedSubtitle.textColor = UIColor.whiteColor()
                        self.rewardSubtitle.textColor = UIColor.whiteColor()
                    }
                    
                }
                else {
                    self.showAlert("Username not found", message: "The username \"\(user)\" was not found in our records. Please check the spelling or contact your Executive for support.")
                }
            }
            
        }

    }
    
    //Alert boxes if account not connected
    func showAlert(title:String, message:String) {
        var inputTextField: UITextField?
        let accountPrompt = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        accountPrompt.addAction(UIAlertAction(title: "Not now", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            self.tabBarController!.selectedIndex = 0;
        }))

        accountPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            var usernameEntered = inputTextField!.text.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            if usernameEntered == ""  {
                self.showAlert("Enter a username", message:"Please enter a career account username.")
            }
            else {
               self.getUserInfo(usernameEntered)
            }
        }))
        accountPrompt.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Purdue Career Account Username"
            inputTextField = textField
        })
        
        presentViewController(accountPrompt, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if events == nil {
            return 0
        }
        else {
            return eventNames.count //Total number of events found
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MyPUDMCellTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MyPUDMCellTableViewCell
        
        cell.eventLabel.text = eventNames[indexPath.row]
        cell.dateLabel.text = eventDates[indexPath.row]
        cell.pointsLabel.text = eventPoints[indexPath.row]
        
        //Change Colors of Text based on Increment and Decrement
        if cell.pointsLabel.text!.toInt() <= 0 {
            cell.pointsLabel.textColor = UIColor.redColor()
        }
        else
        {
            cell.pointsLabel.textColor = self.UIColorFromRGB(0x00965e)
        }
        
        return cell
    }
    
    //When a cell is clicked
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
