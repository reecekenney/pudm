//
//  TableViewController.swift
//  PUDM
//
//  Created by Reece Kenney on 31/03/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, NSXMLParserDelegate, MWFeedParserDelegate {
    
    var parser = NSXMLParser()
    var feeds = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var fTitle = NSMutableString()
    var link = NSMutableString()
    var fDescription = NSMutableString()
    
    var feedItems = [MWFeedItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*self.tableView.rowHeight = 70 //Height of cells
        
        feeds = []
        var url = NSURL(string: "http://www.pudm.org/category/annoucements/feed/")
        parser = NSXMLParser(contentsOfURL: url)!
        parser.delegate = self
        parser.shouldProcessNamespaces = false
        parser.shouldReportNamespacePrefixes = false
        parser.shouldResolveExternalEntities = false
        parser.parse()*/
        
        request()
    }
    
    func feedParserDidStart(parser: MWFeedParser!) {
        feedItems = [MWFeedItem]()
    }
    
    func feedParserDidFinish(parser: MWFeedParser!) {
        self.tableView.reloadData()
    }
    
    func feedParser(parser: MWFeedParser!, didParseFeedInfo info: MWFeedInfo!) {
        self.title = info.title
    }
    func feedParser(parser: MWFeedParser!, didParseFeedItem item: MWFeedItem!) {
        feedItems.append(item)
    }
    
    func request() {
        let url = NSURL(string: "http://en.espnf1.com/rss/motorsport/story/feeds/296.xml?type=2")//"http://www.pudm.org/category/annoucements/feed/")
        let feedParser = MWFeedParser(feedURL: url)
        feedParser.delegate = self
        feedParser.parse()
        
    }
    
    func parser(parser: NSXMLParser!, didStartElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!, attributes attributeDict: [NSObject : AnyObject]!) {
        
        element = elementName
        
        //Instantiate feed properties
        if (element as NSString).isEqualToString("item") {
            elements = NSMutableDictionary.alloc()
            elements = [:]
            fTitle = NSMutableString.alloc()
            fTitle = ""
            link = NSMutableString.alloc()
            link = ""
            fDescription = NSMutableString.alloc()
            fDescription = ""
        }
        
        
    }
    
    func parser(parser: NSXMLParser!, didEndElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!) {
        
        if fTitle != "" {
            elements.setObject(fTitle, forKey: "title")
        }
        if link != "" {
            elements.setObject(link, forKey: "link")
        }
        if fDescription != "" {
            elements.setObject(fDescription, forKey: "description")
        }
        
        feeds.addObject(elements)
        
    }
    
    func parser(parser: NSXMLParser!, foundCharacters string: String!) {
        
        if element.isEqualToString("title") {
            fTitle.appendString(string)
        }
        else if element.isEqualToString("link") {
            link.appendString(string)
        }
        else if element.isEqualToString("description") {
            fDescription.appendString(string)
        }
        
    }
    
    func parserDidEndDocument(parser: NSXMLParser!) {
        self.tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return feedItems.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell
        
        let item = feedItems[indexPath.row] as MWFeedItem
        cell.textLabel?.text = item.title

        /*cell.textLabel?.text = feeds.objectAtIndex(indexPath.row).objectForKey("title") as? String
        cell.detailTextLabel?.numberOfLines = 3
        cell.detailTextLabel?.text = feeds.objectAtIndex(indexPath.row).objectForKey("description") as? String*/

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = feedItems[indexPath.row] as MWFeedItem
        let webBrowser = KINWebBrowserViewController()
        let url = NSURL(string: item.link)
        
        webBrowser.loadURL(url)
        self.navigationController?.pushViewController(webBrowser, animated: true)
    }
    

}
